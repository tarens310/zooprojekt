#ifndef HRAC_H
#define HRAC_H

#include<string>
#include<iostream>

class Hrac {

private:
	int m_PocetGoldu;
	int m_zivoty;
	std::string m_jmeno;

public:
	Hrac(int pocetGoldu, std::string jmeno);
	void printInfo();
	std::string getJmeno();
	void setPocetGoldu(int okolik);
	void setZivot(int okolik);
	int getZivot();
	int getGoldy();
	bool ZivotniStatus();


    ~Hrac();
};
#endif // HRAC_H

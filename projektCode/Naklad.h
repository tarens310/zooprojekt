#ifndef NAKLAD_H
#define NAKLAD_H
#include "Karavana.h"
#include <iostream>

class Naklad {

private:
	int m_objem;
	std::string m_nazev;

public:
	Naklad(std::string nazev, int objem);

	int getObjem();

private:
	Naklad();
};
#endif // NAKLAD_H

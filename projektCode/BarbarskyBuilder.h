#ifndef BARBARSKYBUILDER_H
#define BARBARSKYBUILDER_H
#include "Barbari.h"
class BarbarskyBuilder {

protected:
	Barbari m_barbari;

public:
	void VytvorBarbara(int dovednost);

	Barbari* GetBarbar();
};
#endif // BARBARSKYBUILDER_H

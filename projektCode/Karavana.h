#ifndef KARAVANA_H
#define KARAVANA_H
#include "Hrac.h"
#include "Naklad.h"
#include <iostream>
#include <vector>
class Karavana {


private:
	int m_kapacitaNakladu;
	int m_urovenZbrani;
    //std::vector<Naklad*> m_naklad;
	//std::vector<Hrac*> m_spolecnici;

public:
	Karavana();

	void VypisSpolecniky();
	void PrintInfo();
	void PrijmySpolecniky(Hrac*);
	void ZanechSpolecniky(Hrac*);
	//void NalozNaklad(Naklad*);
	//void VylozNaklad(Naklad*);
    ~Karavana();
};
#endif // KARAVANA_H

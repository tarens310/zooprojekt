#ifndef BARBARI_H
#define BARBARI_H
class Barbari {

private:
	int m_dovednost;

public:
	Barbari(int dovednost);

	void getDovednost();

private:
	Barbari();
};
#endif // BARBARI_H
